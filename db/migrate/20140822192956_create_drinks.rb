class CreateDrinks < ActiveRecord::Migration
  def change
    create_table :drinks do |t|
      t.string :coke
      t.string :beer
      t.string :water

      t.timestamps
    end
  end
end
