class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :meat
      t.string :fish
      t.string :pasta

      t.timestamps
    end
  end
end
