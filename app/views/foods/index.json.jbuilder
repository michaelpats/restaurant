json.array!(@foods) do |food|
  json.extract! food, :id, :meat, :fish, :pasta
  json.url food_url(food, format: :json)
end
